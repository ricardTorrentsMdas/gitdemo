// availity-reactstrap-validation
import PropTypes from 'prop-types'
import React, { Component } from "react"
// Redux
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"
import { Card, CardBody, Col, Container, Row } from "reactstrap"

class HelloWorld extends Component {
  constructor(props) {
    super(props)

  }
 
  
 

 

remember(event){
this.setState({remember:event.target.checked})
}


  render() {
    return (
      <React.Fragment>
        <div className="home-btn d-none d-sm-block">
          <Link to="/" className="text-dark">
            <i className="bx bx-home h2" />
          </Link>
        </div>
        <div className="account-pages my-5 pt-sm-5">
          <Container>
            <Row className="justify-content-center">
              <Col md={8} lg={6} xl={5}>
                <Card className="overflow-hidden">
                  <div className="bg-primary bg-soft">
                    <Row>
                      <Col className="col-7">
                        <div className="text-primary p-4">
                        <h5 id="welcomeback" className="text-primary">Hello world !!</h5>
                        </div>
                      </Col>
                      
                    </Row>
                  </div>
                  <CardBody className="pt-0">

                        </CardBody>
                </Card>
       </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    )
  }
}

HelloWorld.propTypes = {

  error: PropTypes.any,
  history: PropTypes.object,

  socialLogin: PropTypes.func
}

const mapStateToProps = state => {
 
  return {  }
}

export default withRouter(
  connect(mapStateToProps, {})(HelloWorld)
)
